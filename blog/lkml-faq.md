---
authors: tommie
title: About the LKML FAQ
date: 2021-08-29T01:01:03+00:00
tags: ["kernel", "linux"]
draft: false
hide_table_of_contents: false
toc_min_heading_level: 2
toc_max_heading_level: 3
---

One of the archive pages of the Linux Kernel Mailing List is LKML.org.  

<!-- truncate -->

https://lkml.org

And at the bottom of the homepage a link directs visitors to [the FAQ][01].
This post discusses some observations about that page.  

## Tux website  
The second paragraph has a link to the "official" website of the LKML. However, that link, when
opened in Firefox, sends you to a blank page. With `curl`, we get the following:  

```
$ curl -i https://www.tux.org/lkml
HTTP/1.0 500 Internal Server Error
Date: Mon, 30 Aug 2021 08:43:25 GMT
Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/5.4.16
X-Powered-By: PHP/5.4.16
X-Content-Type-Options: nosniff
Content-Length: 0
Connection: close
Content-Type: text/html; charset=UTF-8
```

What the `-i` does in `curl` is, according to the `man` page, `include the HTTP response headers in
the output`. Since the command shows a `500 Internal Server Error`, we can assume that webpage is
not being maintained anymore.  

In that same paragraph, the FAQ lists links to some available mirrors. Let's check out one of them:  

```
$ curl -i https://www.atnf.csiro.au/~rgooch/linux/docs/lkml/ | head
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:--  0:00:01 --:--:--     0HTTP/1.1 404 Not Found
Date: Mon, 06 Sep 2021 18:16:01 GMT
Server: Apache
X-Drupal-Cache: HIT
Content-Language: en
X-Frame-Options: sameorigin
Permissions-Policy: interest-cohort=()
X-Generator: Drupal 7 (http://drupal.org)
Link: <https://www.atnf.csiro.au/>; rel="canonical",<https://www.atnf.csiro.au/>; rel="shortlink"
Cache-Control: public, max-age=86400
```

[01]: http://vger.kernel.org/lkml "FAQ of the LKML"
