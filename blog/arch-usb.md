---
authors: tommie
title: Create a live USB with manual formatting
date: 2022-09-27T20:01:03-03:00
tags: ["kernel", "linux"]
draft: false
hide_table_of_contents: false
toc_min_heading_level: 2
toc_max_heading_level: 3
#slug: arch-usb
---

To install Arch Linux (Arch) in your computer, you first need to obtain a live USB drive.  

<!-- truncate -->

https://wiki.archlinux.org/title/USB_flash_installation_medium

One of the most common ways to do it is [using][01] `dd` or `cat`, in which
case your USB drive will be basically unusable as a permanent storage device,
and only the ArchLinux ISO file will be inside it. 

If you want to still be able to use your USB drive as a storage device, you can go to the section titled ["Manual formatting"][02]
in the ArchLinux wiki. However, the steps are not always too clear. If you don't do the partitioning correctly you might get this
error right after the GRUB screen when booting:  

<!-- https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code -->
<!-- https://highlightjs.org/static/demo/ -->
<!-- https://gohugo.io/content-management/formats/ -->

```Less
error: invalid magic number
error: you need to load the kernel first
```

Some common results on the web about this error:

- [ArchLinux BBS: Invalid magic number](https://bbs.archlinux.org/viewtopic.php?id=178314)
- [Arch BBS: Invalid magic number on boot](https://bbs.archlinux.org/viewtopic.php?id=266864)
- [Linux.org: Installing problem](https://www.linux.org/threads/solved-installing-problem.39854/)
- [Clonezilla discussion](https://sourceforge.net/p/clonezilla/discussion/Clonezilla_live/thread/fb77f6ae6e/?limit=25#0774)
- [Reddit: Invalid magic 
number](https://i.reddit.com/r/linux4noobs/comments/96dktl/invalid_magic_number_and_you_need_to_load_the/)

In summary, many comments say that it's a hardware problem, and that you should get a new USB drive, which costs money. However,
sometimes it can be solved just by doing the partitioning correctly.  

The ArchLinux wiki says: 

> If not done yet, create a partition table on `/dev/sdX`.  
> If not done yet, create a partition on the device. The partition `/dev/sdXn` must be formatted to FAT32.

Create the partition table with `parted`:  

```Shell Session
$ sudo parted /dev/sdb
(parted) print free
(parted) mklabel gpt
(parted) quit
$ sudo fdisk -l
```

Then create the partition with `cfdisk`:

```Shell Session
$ sudo cfdisk /dev/sdb
```

Select `New`, then type in the desired size. For the 202209 (September 2022) version, a size of 850MiB should be enough. Then
select `Type`. And this is the important part: you have to select `EFI system`. Then select `Write`. Then `Quit`. Then you can
format the partition to FAT32. 

```Shell Session
$ sudo mkfs.fat -F 32 /dev/sdb1
```

After that, you can continue following the instructions of the ArchLinux wiki. 

[00]: https://wiki.archlinux.org/title/USB_flash_installation_medium 
[01]: https://wiki.archlinux.org/title/USB_flash_installation_medium#Using_the_ISO_as_is_(BIOS_and_UEFI) 
[02]: https://wiki.archlinux.org/title/USB_flash_installation_medium#Using_manual_formatting 
[10]: https://lkml.org "LKML archive website"
[11]: http://vger.kernel.org/lkml "FAQ of the LKML"
