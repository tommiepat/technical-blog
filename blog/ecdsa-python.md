---
authors: tommie
title: ECDSA in Python
date: 2022-09-30T02:49:03-03:00
tags: ["python", "ecdsa"]
draft: false
hide_table_of_contents: false
toc_min_heading_level: 2
toc_max_heading_level: 3
#slug: ecdsa-python
---

The ECDSA is a variant of the Digital Signature Algorithm which uses elliptic-curve cryptography.  

<!-- truncate -->

https://en.wikipedia.org/wiki/Elliptic_Curve_Digital_Signature_Algorithm

Starkbank has an open-source repository for a pure-Python ECDSA.  
https://github.com/starkbank/ecdsa-python

Let's take a look at the `.travis.yml` file. 

```yml
script:
  - python -m unittest discover
```

When we run that with Python 3.10.7, we get: 

```Python
......EE...........
======================================================================
ERROR: testAssign (tests.testOpenSSL.OpensslTest)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/starkbank/ecdsa-python/tests/testOpenSSL.py", line 9, in testAssign
    privateKeyPem = File.read("privateKey.pem")
  File "/home/starkbank/ecdsa-python/ellipticcurve/utils/file.py", line 7, in read
    with open(path, mode) as blob:
FileNotFoundError: [Errno 2] No such file or directory: 'privateKey.pem'

======================================================================
ERROR: testVerifySignature (tests.testOpenSSL.OpensslTest)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/starkbank/ecdsa-python/tests/testOpenSSL.py", line 24, in testVerifySignature
    publicKeyPem = File.read("publicKey.pem")
  File "/home/starkbank/ecdsa-python/ellipticcurve/utils/file.py", line 7, in read
    with open(path, mode) as blob:
FileNotFoundError: [Errno 2] No such file or directory: 'publicKey.pem'

----------------------------------------------------------------------
Ran 19 tests in 16.225s

FAILED (errors=2)
```

So it looks like we have to get some files from the `tests` directory: 

```Bash
$ cp tests/p*Key.pem tests/message.txt tests/signatureDer.txt .
$ python -m unittest discover
...................
----------------------------------------------------------------------
Ran 19 tests in 16.127s

OK
```

Now let's look at the other lines in `.travis.yml`:

```yml
language: python

matrix:
  include:
    - python: "2.7"
    - python: "3.4"
    - python: "3.5"
    - python: "3.6"
    - python: "3.7"
    - python: "3.8"
    - python: "3.9-dev"
    - python: "pypy2.7-6.0"
    - python: "pypy3.5-6.0"
  allow_failures:
    - python: "3.9-dev"
```

As of October 2022, the latest active Python release is 3.10, with bugfix status; while the latest pre-release testing version is
[3.11.0rc2][02]. As we saw with the tests run above, the tests were successfully run without errors on Python 3.10.7 , so a
suggestion would be to add 3.9, 3.10, and 3.11-dev to the Python versions listed on the `.travis.yml` file, and to add 3.11-dev to the
`allow_failures` section. If Travis accepts the 3.11 version (without `-dev`), then that could be considered too. 

[02]: https://www.python.org/downloads/release/python-3110rc2/

Note: Wikiwand links were removed from this page. See:  
https://old.reddit.com/r/privacy/comments/m9ugpu/psa_the_browser_extension_wikiwand_is_collecting/
