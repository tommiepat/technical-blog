---
authors: tommie
title: Moving Pass to a New Computer
date: 2022-09-29T19:51:03-03:00
tags: ["security", "gpg"]
draft: false
hide_table_of_contents: false
toc_min_heading_level: 2
toc_max_heading_level: 3
---

Pass is a simple and secure password manager.  

<!-- truncate -->

https://www.passwordstore.org/

If you use Pass regularly, and have to move to a new machine, you can keep all
your passwords in Pass. You can also check out DerrickGee's [guide][00]. 

## GPG keys
You'll need your user id, and to find it, you can use gpg:  

```Shell Session
$ gpg --list-secret-keys
```

You will see your user-id right before your email address.  
On your old machine, [export][02] your secret keys, also called "private keys". 
<!-- https://stackoverflow.com/questions/51501686/markdown-on-github-how-to-do-italics-inside-code-blocks-triple-backticks -->
<pre><code>$ gpg --export-secret-keys --armor --output <i>privkey.asc user-id</i></code></pre>

Here, `armor` means the generated file will be in ASCII armor, which can be used for printing out the file in physical paper. You
can replace `privkey.asc` with any name you want, for example `myPrivateExport.asc`. You have to replace `user-id` with your user
id.  
Note that, if you run the above command a second time, then you'll get a new file with
contents that are different from the file you got from the first run.  
On your old machine, you'll also have to export your public [key][01]. 

<pre><code>$ gpg --export--armor --output <i>public.key user-id</i></code></pre>

You will also have to find your Pass directory, which by default is `~/.password-store`. Then you can copy all those files into
your USB drive. 

```
$ sudo mount /dev/sdb1 /mnt/
$ sudo cp -r ~/privkey.asc ~/public.key ~/.password-store/ /mnt/
$ sudo umount /dev/sdb1
```

Now on the new machine: 

```
$ sudo mount /dev/sdb1 /mnt/
$ sudo cp -r /mnt/* ~/
$ sudo umount /dev/sdb1
```

Now import both keys:

```
$ gpg --import public.key
$ gpg --import privkey.asc
```

When importing the private key, gpg will display a password input box or dialog box, where you can
securely enter the password.  
Then edit the [key][03]:

```
$ gpg --edit-key <KEY_ID_or_USER_ID>
gpg> trust
```

[Select][04] number 5 for `ultimately`, then quit.

```
gpg> quit
```

If you are working in a temporary directory, then make sure that at the end of the process the
`$HOME/.password-store` or `~/.password-store` directory exists, and that it's located in your
`$HOME` directory. That's what `pass` reads, as documented in its man page.  
[https://git.zx2c4.com/password-store/about/](https://git.zx2c4.com/password-store/about/)

Now you can finally start using Pass with all your old passwords: 

```
$ pass
$ pass myOldDir/myOldUsername
$ pass insert myNewDir/myNewUsername
```

## Expiration date
To change the expiration date of a key:  

```
$ gpg --list-secret-keys
$ gpg --edit-key user-id
gpg> key 0
gpg> expire
Key is valid for? (0) 1y
Is this correct? (y/N) y
gpg> save
gpg> key 1
gpg> expire
Key is valid for? (0) 1y
Is this correct? (y/N) y
gpg> save
```

See also:  
[https://superuser.com/questions/1371088/what-do-ssb-and-sec-mean-in-gpgs-output](https://superuser.com/questions/1371088/what-do-ssb-and-sec-mean-in-gpgs-output)

## Troubleshooting

### No secret key
If you copy only the `.password-store/` directory without importing any key, running `pass` will return the following [error][05]:

```Bash
gpg: decryption failed: No secret key
```

[00]: https://antisyllogism.medium.com/password-manager-pass-importing-and-exporting-b206a7eaaa70
[01]: https://wiki.archlinux.org/title/GnuPG#Export_your_public_key
[02]: https://wiki.archlinux.org/title/GnuPG#Backup_your_private_key
[03]: https://stackoverflow.com/questions/33361068/gnupg-there-is-no-assurance-this-key-belongs-to-the-named-user
[04]: https://wiki.archlinux.org/title/Pass#Basic_usage
[05]: https://stackoverflow.com/questions/28321712/gpg-decryption-fails-with-no-secret-key-error

### No valid encryption subkey
After setting up `expire` in gpg, you get the message:  

```
gpg: WARNING: No valid encryption subkey left over.
```

, and no expire date gets changed. That happens when you try to extend the expire date of your
Secret Subkey (ssb) before extending the expire date of your Secret Key (sec). To solve that,
first ran `expire`, then `key 0`, then `save`, then `key 1` (the number depends on the number
corresponding to your ssb), then `expire`, and finally `save`.  
