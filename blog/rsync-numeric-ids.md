---
authors: tommie
title: Copying the HOME directory
date: 2024-10-17T21:00:00-03:00
tags: ["rsync"]
draft: false
hide_table_of_contents: false
toc_min_heading_level: 2
toc_max_heading_level: 3
---

With `rsync`, you can move copy your `$HOME` directory to a new system or computer.  

<!-- truncate -->

```Shell Session
$ rsync --numeric-ids -aiHAX /path/to/source /path/to/dest
```

That is equivalent to:  
```Shell Session
$ rsync --numeric-ids --archive --itemize-changes --hard-links --acls --xattrs /path/to/source /path/to/dest
```

