---
authors: tommie
title: Shells and Neovim
date: 2022-09-28T11:01:00-03:00
tags: ["shell", "spellcheck"]
draft: false
hide_table_of_contents: false
toc_min_heading_level: 2
toc_max_heading_level: 3
---

We'll talk about some shells and Neovim.  

<!-- truncate -->

This [article][00] (HN [discussion][01]) recommends Xmonad because that window manager has been around since 2007, however, xfce
has been around for longer, and is more convenient to install. But Xmonad is very customizable, and you can, for example, use
[NamedScratchpads][05].  This [repository][06] from Derek Taylor aka DistroTube contains some interesting dot files. 

As for the shell, he praises bash, however, fish sometimes have nice [features][04]. In
fish, you can use `abbr` to abbreviate commands. You can also create a `function`, e.g. a `ddg` function that searches DuckDuckGo. 

This other [article][02] (HN [discussion][03]) talks about Vale and Neovim. In Neovim, you can use the null-ls plugin to use Vale.  

In urxvt 9.30, there is a [bug][07] where a warning message appears when you start urxvt from another terminal. 

# Misc

## Pacman in ArchLinux
In ArchLinux, it's not recommended to run `pacman -Sy package_name` because that would be
considered a partial upgrade. Therefore, running `pacman -Sy` would let you unabe to install
packages, unless you run `pacman -Syu`, which in itself would require you to reboot your PC. If
you don't want to reboot, but still want to see the current or latest versions of packages
available in the main ArchLinux repos, then you can use these commands:

```Shell session
$ tmpdir=$(mktemp -d)
$ fakeroot pacman --dbpath=$tmpdir -Sy
$ pacman --dbpath=$tmpdir -Si archlinux-keyring
```

## Gentoo binaries
To check whether a package you have installed is a binary version, you can look for a file
named `QA_PREBUILT` in `/var/db/pkg/categoryName/packageName/`. If the file is present, then that
means the installed package is a binary one. Some packages are denoted as `packageName-bin`
when they are binaries, but not all of them are. 

## IRC
In IRC, to see the list of users banned from a channel you can do `/mode #channelName +b`. 

## Windows
United States International with AltGr dxxd keys: 
https://github.com/thomasfaingnaert/win-us-intl-altgr

How to bypass the account requirement during OS installation: 
https://www.elevenforum.com/t/what-is-oobe-bypassnro.5011/#post-98972

[00]: https://tylercipriani.com/blog/2022/06/15/choose-boring-desktop-technology/ 
[01]: https://news.ycombinator.com/item?id=31769604 "Cool desktops don't change"
[02]: https://bhupesh.me/writing-like-a-pro-with-vale-and-neovim/
[03]: https://news.ycombinator.com/item?id=31780190 "Vale and Neovim"
[04]: https://fishshell.com/docs/current/cmds/abbr.html "Fish shell abbr"
[05]: https://hackage.haskell.org/package/xmonad-contrib-0.13/docs/XMonad-Util-NamedScratchpad.html
[06]: https://gitlab.com/dwt1/dotfiles
[07]: https://github.com/FortAwesome/Font-Awesome/issues/3681#issuecomment-255146498
