// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

//https://github.com/facebook/docusaurus/issues/8940#issuecomment-1532573496
//const lightCodeTheme = require('prism-react-renderer/themes/github');
//const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const lightCodeTheme = require('prism-react-renderer').themes.github;
const darkCodeTheme = require('prism-react-renderer').themes.dracula;

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Tech blog',
  tagline: '',
  favicon: 'img/mainboard-32x32.jpg',

  ////https://github.com/praveenn77/docusaurus-lunr-search#how-to-use-
  //plugins: [require.resolve('docusaurus-lunr-search')],

  // Set the production url of your site here
  url: 'https://tommiepat.gitlab.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/technical-blog',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: false,
        //docs: {
        //  sidebarPath: require.resolve('./sidebars.js'),
        //  //editUrl:
        //  //  'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        //},
        blog: {
          routeBasePath: '/',
          showReadingTime: true,
          showLastUpdateAuthor: false,
          showLastUpdateTime: true,
          //editUrl:
          //  'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // ...
      algolia: {
        // The application ID provided by Algolia
        appId: 'Y5PI7Z6L3R',
  
        // Public API key: it is safe to commit it
        apiKey: '79cdae37c26890d8555486c33da27319',
  
        indexName: 'tommiepat-gitlab',
  
        // Optional: see doc section below
        contextualSearch: true,
  
        // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
        //externalUrlRegex: 'external\\.com|domain\\.com',
  
        // Optional: Replace parts of the item URLs from Algolia. Useful when using the same search index for multiple deployments using a different baseUrl. You can use regexp or string in the `from` param. For example: localhost:3000 vs myCompany.com/docs
        //replaceSearchResultPathname: {
        //  from: '/docs/', // or as RegExp: /\/docs\//
        //  to: '/',
        //},
  
        // Optional: Algolia search parameters
        searchParameters: {},
  
        // Optional: path for search page that enabled by default (`false` to disable it)
        //searchPagePath: 'search',
  
        //... other Algolia params
      },
      //https://docusaurus.io/docs/api/themes/configuration#color-mode---dark-mode 
      colorMode: {
        //Default is light
        defaultMode: 'dark',
        //Default is false
        disableSwitch: false,
        //Default is false
        respectPrefersColorScheme: false,
      },
      image: 'img/mainboard-180x180.jpg',
      navbar: {
        title: 'Blog',
        logo: {
          alt: 'Mainboard Logo',
          src: 'img/mainboard-32x32.jpg',
        },
        items: [
          //{
          //  type: 'docSidebar',
          //  sidebarId: 'tutorialSidebar',
          //  position: 'left',
          //  label: 'Tutorial',
          //},
          //{to: '/blog', label: 'Blog', position: 'left'},
          {to: '/', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/tommiepat/technical-blog',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          //{
          //  title: 'Docs',
          //  items: [
          //    {
          //      label: 'Tutorial',
          //      to: '/docs/intro',
          //    },
          //  ],
          //},
          {
            title: 'Community',
            items: [
              {
                label: 'Stack Overflow',
                href: 'https://stackoverflow.com/questions/tagged/docusaurus',
              },
              {
                label: 'Discord',
                href: 'https://discordapp.com/invite/docusaurus',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/tommiepat',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Tommie, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
